package accessor

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

type dbAccessor struct {
	db *gorm.DB
}

// NewDBAccessor create a new DB accessor.
func NewDBAccessor(db *gorm.DB) (Accessor, error) {
	if err := db.DB().Ping(); err != nil {
		return nil, errors.Wrap(err, "check connection")
	}

	return &dbAccessor{db: db}, nil
}
