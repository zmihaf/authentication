export GO111MODULE=on
help: ## This help dialog.
help h:
	@IFS=$$'\n' ; \
	help_lines=(`fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##/:/'`); \
	printf "%-20s %s\n" "target" "help" ; \
	printf "%-20s %s\n" "------" "----" ; \
	for help_line in $${help_lines[@]}; do \
		IFS=$$':' ; \
		help_split=($$help_line) ; \
		help_command=`echo $${help_split[0]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
		help_info=`echo $${help_split[2]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
		printf '\033[36m'; \
		printf "%-20s %s" $$help_command ; \
		printf '\033[0m'; \
		printf "%s\n" $$help_info; \
	done

# def
GO = go
BIN_PATH = bin
GOFLAGS = -p 4 -v
BUILD = $(GO) build $(GOFLAGS) -o
DOWNLOAD_DEPENDENCY = $(GO) mod download

server_build_path = cmd/server

# build
COMPONENT = $@
# this variable releated to the Jenkins run part
BIN_ALIAS = $(COMPONENT)
BIN = $(BIN_PATH)/$(BIN_ALIAS) # binary name
BUILD_PATH = $($(COMPONENT)_build_path)

all: ## Make all components
all a: server

server: ## Build server

server: dependency
	@mkdir -p $(BIN_PATH)
	$(BUILD) $(BIN) $(BUILD_PATH)/*.go

.PHONY: clean
clean: ## clean executables
	rm -rf $(BIN_PATH)/*

.PHONY: dependency
dependency: ## download dependency
	$(DOWNLOAD_DEPENDENCY)

.PHONY: vet
vet: ## go vet files
	@$(GO) vet $(GOFLAGS) ./...
