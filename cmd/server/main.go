package main

import (
	"os"
	
	"gitlab.com/kawalcovid19/kawal-diri/authentication/cmd"

	"github.com/golang/glog"
	"github.com/urfave/cli/v2"
)

func main() {
	app := cli.App{
		Name:     "server",
		HelpName: "Authentication Server",
		Version:  "1.0.0",
		Usage:    "Authentication Server",
	}

	if err := cmd.WrappedRunApp(app, os.Args, run); err != nil {
		glog.Fatalf("error running authentication server: %s", err)
	}
}
