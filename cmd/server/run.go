package main

import (
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"gitlab.com/kawalcovid19/kawal-diri/authentication/accessor"
	"gitlab.com/kawalcovid19/kawal-diri/authentication/service"
	service_proto "gitlab.com/kawalcovid19/kawal-diri/authentication/service/proto"

	"github.com/golang/glog"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"google.golang.org/grpc"
	"github.com/joho/godotenv"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" //postgres database driver

)

func run(ctx *cli.Context) error {
	var err error
	err = godotenv.Load()
	if err != nil {
		return errors.Wrap(err, "Error getting env")
	}

	grpcPortString := os.Getenv("GRPC_PORT")
	grpcPort, err := strconv.Atoi(grpcPortString)
	if err != nil {
		return errors.Wrap(err, "invalid GRPC_PORT environment variable")
	}

	httpPortString := os.Getenv("HTTP_PORT")
	httpPort, err := strconv.Atoi(httpPortString)
	if err != nil {
		return errors.Wrap(err, "invalid HTTP_PORT environment variable")
	}

	if grpcPort <= 0 || httpPort <= 0 {
		return fmt.Errorf("invalid port [grpc: %d, http: %d]", grpcPort, httpPort)
	}

	dbHostString := os.Getenv("DB_HOST")
	dbPortString := os.Getenv("DB_PORT")
	dbUserString := os.Getenv("DB_USER")
	dbNameString := os.Getenv("DB_NAME")
	dbPasswordString := os.Getenv("DB_PASSWORD")

	dbConn := fmt.Sprintf("host=%v port=%v user=%v dbname=%v password=%v", dbHostString, dbPortString, dbUserString, dbNameString, dbPasswordString)
	db, err := gorm.Open(os.Getenv("DB_DRIVER"), dbConn)
	if err != nil {
		return errors.Wrap(err, "connect database")
	}
	ac, err := accessor.NewDBAccessor(db)
	if err != nil {
		return errors.Wrap(err, "create DB accessor")
	}

	s, err := service.NewAuthService(ac)
	if err != nil {
		return errors.Wrap(err, "create auth service")
	}

	errChan := make(chan error, 1)

	go func() {
		lis, err := net.Listen("tcp", fmt.Sprintf(":%d", grpcPort))
		if err != nil {
			errChan <- errors.Wrap(err, "listening tcp for grpc")
		}

		glog.Infof("Running GRPC Server at port %d", grpcPort)
		server := grpc.NewServer()
		service_proto.RegisterAuthenticationServer(server, s)
		if err := server.Serve(lis); err != nil {
			errChan <- errors.Wrap(err, "serving grpc server")
		}
	}()

	go func() {
		mux := runtime.NewServeMux()
		if err := service_proto.RegisterAuthenticationHandlerServer(ctx.Context, mux, s); err != nil {
			errChan <- err
		}

		glog.Infof("Running HTTP Server at port %d", httpPort)
		if err := http.ListenAndServe(fmt.Sprintf(":%d", httpPort), mux); err != nil {
			errChan <- errors.Wrap(err, "listening and serving http server")
		}
	}()

	sigChannel := make(chan os.Signal, 1)
	signal.Notify(sigChannel, os.Interrupt, syscall.SIGTERM)

	select {
	case err := <-errChan:
		return err
	case sig := <-sigChannel:
		glog.Infof("Signal %s received, exiting", sig)
	}

	return nil
}
