package cmd

import (
	"github.com/joho/godotenv"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
)

type CLIRunnerFunc func(*cli.Context) error

// WrappedRunApp runs a CLI app
func WrappedRunApp(app cli.App, arguments []string, run CLIRunnerFunc) error {
	app.Flags = append(app.Flags, &cli.StringSliceFlag{
		Name:    "config",
		Aliases: []string{"c"},
		Usage:   "Config file path. Accept multiple files.",
		Value:   cli.NewStringSlice(".env"),
	},
	)

	app.Action = func(c *cli.Context) error {
		if err := godotenv.Load(c.StringSlice("config")...); err != nil {
			return errors.Wrap(err, "load configuration file")
		}

		return run(c)
	}

	return app.Run(arguments)
}
