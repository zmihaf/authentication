package service

import (
	"context"

	"gitlab.com/kawalcovid19/kawal-diri/authentication/accessor"
	service_proto "gitlab.com/kawalcovid19/kawal-diri/authentication/service/proto"
)

type authService struct {
	ac accessor.Accessor
}

// NewAuthService create a new auth service.
func NewAuthService(ac accessor.Accessor) (service_proto.AuthenticationServer, error) {
	return &authService{ac: ac}, nil
}

func (a *authService) LoginByGoogle(context.Context, *service_proto.LoginByGoogleRequest) (*service_proto.LoginResponse, error) {
	panic("implement me")
}

func (a *authService) LoginByFacebook(context.Context, *service_proto.LoginByFacebookRequest) (*service_proto.LoginResponse, error) {
	panic("implement me")
}

func (a *authService) SignUp(context.Context, *service_proto.User) (*service_proto.SignUpResponse, error) {
	panic("implement me")
}

func (a *authService) GetUser(context.Context, *service_proto.GetUserRequest) (*service_proto.GetUserResponse, error) {
	panic("implement me")
}
