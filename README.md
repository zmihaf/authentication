# Kawal Diri Authentication Service
**See overall Kawal Diri engineering architecture [here](https://gitlab.com/kawalcovid19/kawal-diri/docs)**

These stack using Go 1.14, PostgreSql 11

To contribute, use git clone `this repository`
- Find Open ticket and Issues list
- create branch name with prefix or suffix as ticket name
- run make help: to get information
- run make all && make dependency && make server
- run `cp .env.example .env`
- run executable file on `bin/server`
- create Merge Request and wait at least 1 reviewer

Thank you for contributing




